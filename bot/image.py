from PIL import Image as PilImage
from datetime import datetime
from exif import Image as ExifInfo
from typing import Tuple
from tempfile import mktemp
from os import remove
from io import BytesIO


class Image:
    def __init__(self, data: bytes):
        self._file = mktemp()
        with open(self._file, "wb") as file:
            file.write(data)
        self._image = PilImage.open(self._file)

    def __del__(self):
        self._image.close()
        remove(self._file)

    def size(self) -> Tuple[int, int]:
        return self._image.size

    def datetime(self) -> datetime:
        with open(self._file, "rb") as file:
            exif = ExifInfo(file)
            dt = exif.get("datetime_original")
            return datetime.strptime(dt, "%Y:%m:%d %H:%M:%S")

    def resize(self, max_d: int) -> PilImage:
        image = self._image.copy()
        image.thumbnail((max_d, max_d))
        return image
