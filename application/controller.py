from dateutil.parser import parse as parse_date, ParserError
from datetime import date
from typing import Iterable, Set
from .models import Photo


def _search_by_tags(tags: Set[str]) -> Iterable:
    tags_no_sharp = [tag.replace("#", "") for tag in tags]
    return Photo.objects.filter(tags__contains=tags_no_sharp)


def _search_by_date(_date: date) -> Iterable:
    return Photo.objects.filter(date__year=_date.year,
                                date__month=_date.month,
                                date__day=_date.day)


def _search_by_name(name: str) -> Iterable:
    # TODO: case-insensitive
    return Photo.objects.filter(name__contains=name)


def _all() -> Iterable:
    return Photo.objects.all()


def search_photos(request: str) -> Iterable:
    words = request.strip().split()
    # If request is empty - user wants for all photos
    if not words:
        return _all()

    # If every word starts with "#" - user want for tags
    if all([w.startswith("#") for w in words]):
        return _search_by_tags(set(words))

    # If user passed valid date - user want for date
    try:
        parsed_date = parse_date(request)
        if parsed_date:
            return _search_by_date(parsed_date.date())
    except ParserError:
        pass

    # Maybe user wants for name?
    name_search = _search_by_name(request)
    if name_search:
        return name_search

    # Ok, that is tags
    return _search_by_tags(set(words))
