from django.shortcuts import render, redirect
from .models import Photo
from .controller import search_photos
from typing import Iterable, Dict


def _split_photos(photos: Iterable) -> Dict:
    return {"left_photos": photos[::2], "right_photos": photos[1::2]}


def index(request):
    photos = Photo.objects.all()
    return render(request, "index.html", _split_photos(photos))


def search(request):
    ts = request.GET["search"]

    photos = search_photos(ts)

    return render(request, "index.html", _split_photos(photos))
