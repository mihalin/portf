from django.db import models
from django.contrib.postgres.fields import ArrayField


class Photo(models.Model):
    date = models.DateTimeField()
    name = models.TextField(unique=True)
    tags = ArrayField(models.TextField(), default=list)

