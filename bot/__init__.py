import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "portf.portf.settings")
django.setup()
from portf.application.models import Photo
from portf.bot.image import Image


if __name__ == '__main__':
    import django
    import os

    with open("bot/4.jpg", "rb") as file:
        image = Image(file.read())

    dt = image.datetime()
    code = int(dt.timestamp())
    tags = ["new", "hi"]

    image.resize(1024).save(f"/home/walker/Projects/portf/portf/application/static/images/fulls/{code}.jpg")
    image.resize(360).save(f"/home/walker/Projects/portf/portf/application/static/images/thumbs/{code}.jpg")

    Photo.objects.create(date=dt, name=code, tags=tags)

